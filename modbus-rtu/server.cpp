#include "modbus/modbus.h"

#include <iostream>
#include <thread>

int main() {
    modbus_t *ctx;
    ctx = modbus_new_rtu("/dev/ttyUSB0", 9600, 'N', 8, 1);

    if (modbus_set_debug(ctx, true)) {
        fprintf(stderr, "Unable to set debug\n");
        return -1;
    }

    if (ctx == nullptr) {
        fprintf(stderr, "Unable to create the libmodbus context\n");
        return -1;
    }

    if (modbus_set_slave(ctx, 0x1)) {
        fprintf(stderr, "Unable to set slave\n");
        return -1;
    }

    if (modbus_connect(ctx) == -1) {
        fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
        modbus_free(ctx);
        return -1;
    }

    const uint8_t raw_req[] = {0x01, 0xA, 0x00, 0x01, 0x01};
    int raw_req_lenght = sizeof(raw_req);
    auto *query = new uint8_t[MODBUS_RTU_MAX_ADU_LENGTH];
    auto rc = 0;

    modbus_set_response_timeout(ctx, 2000, 2000);

    while (true) {
        if (modbus_send_raw_request(ctx, raw_req, raw_req_lenght) == -1) {
            fprintf(stderr, "Unable to write raw\n");
            break;
        }

        do {
            rc = modbus_receive(ctx, query);
        } while (rc == 0);

        if (rc < 0) {
            perror("Error in modbus receive");
        }
        std::cout << "rc = " << rc << std::endl;
        std::cout << query << std::endl;

        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }


    modbus_close(ctx);
    modbus_free(ctx);
}

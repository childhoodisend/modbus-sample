#include "modbus_commands.h"
#include "modbus_msg.h"
#include "globals.h"

#include <iostream>
#include <algorithm>
#include <string>

namespace modbus_conv {

void set_frame(modbus_msg_t &dst) {

    dst.header.clear();
    dst.data.clear();
    dst.crc.clear();

    dst.header.reserve(2);

    std::string comm{};
    std::getline(std::cin, comm);
    std::transform(comm.begin(), comm.end(), comm.begin(), ::tolower);

    if(set_commands.find(comm) != set_commands.end()) {

        switch (set_commands[comm]) {
            case 0:
                comm_set_slave(dst);
                break;
            case 1:
                comm_set_baud(dst);
                break;
            case 2:
                comm_set_type_motor(dst);
                break;
            case 3:
                comm_set_availability_stopper(dst);
                break;
            case 4:
                comm_set_end_sensors(dst);
                break;
            case 5:
                comm_set_availability_increment_encoder(dst);
                break;
            case 6:
                comm_set_availability_absolute_encoder(dst);
                break;
            case 7:
                comm_set_availability_potentiometer(dst);
                break;
            case 8:
                comm_set_microstep_mode(dst);
                break;
            case 9:
                comm_set_maximum_speed_BLDC(dst);
                break;
            case 10:
                comm_set_boost_speed_BLDC(dst);
                break;
            case 11:
                comm_set_quantity_tact_encoder(dst);
                break;
            case 12:
                comm_set_brake_condition(dst);
                break;
            case 13:
                comm_set_reduction_level(dst);
                break;
        }
    }

    if (req_commands.find(comm) != req_commands.end()) {
        switch (req_commands[comm]) {
            case 0:
                comm_req_calibration(dst);
                break;
            case 1:
                comm_req_firmware_and_configuration(dst);
                break;
            case 2:
                comm_req_diagnostic(dst);
                break;
            case 3:
                comm_req_read_id(dst);
                break;
            case 4:
                comm_req_quest_condition_sensor(dst);
                break;
        }
    }

    if (proc_commands.find(comm) != proc_commands.end()) {
        switch (proc_commands[comm]) {
            case 0:
                comm_proc_displacement_in_position(dst);
                break;
            case 1:
                comm_proc_start_stop_velocity(dst);
                break;
        }
    }

    //TODO add crc
}

//*****SET COMMANDS*****

void set_frame(modbus_msg_t &dst, const std::vector<uint8_t> &src, bool crc_required) {
    if (src.empty()) {
        throw std::runtime_error("set_frame() exc : scr is empty!");
    }

    dst.header.clear();
    dst.data.clear();
    dst.crc.clear();

    size_t n_bytes = src.size();

    // TODO check n_bytes
    if (crc_required) {
        if (n_bytes > 4) {
            dst.header.reserve(2);
            dst.data.reserve(n_bytes - 4);
            dst.crc.reserve(2);

            for (size_t i = 0; i < 2; ++i) {
                dst.header.push_back(src[i]);
            }
            for (size_t i = 2; i < n_bytes - 2; ++i) {
                dst.data.push_back(src[i]);
            }
            for (size_t i = n_bytes - 2; i < n_bytes; ++i) {
                dst.crc.push_back(src[i]);
            }
        }
        else {
            throw std::runtime_error("set_frame() exc : crc is required, but n_bites <= 4");
        }
    }
    else {
        if (n_bytes > 2) {
            dst.header.reserve(2);
            dst.data.reserve(n_bytes - 2);

            for (size_t i = 0; i < 2; ++i) {
                dst.header.push_back(src[i]);
            }
            for (size_t i = 2; i < n_bytes; ++i) {
                dst.data.push_back(src[i]);
            }
        }
        else {
            throw std::runtime_error("set_frame() exc : crc is not required, but n_bites <= 2");
        }
    }
}

void comm_set_slave(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_slave() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 10}); // [cur, 0x0A]

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_slave() invalid " + std::to_string(cur));
    }
    set_data(dst, {0, 1, (uint8_t)cur}); // [0x00, 0x01, cur]
}

void comm_set_baud(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_baud() invalid " + std::to_string(cur));
    }
    modbus_conv::set_header(dst, {(uint8_t)cur, 10}); // [cur, 0x0A]

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 9) {
        throw std::runtime_error("comm_set_baud() invalid " + std::to_string(cur));
    }
    set_data(dst, {255, 0, (uint8_t)cur}); // [0xFF, 0x00, cur]
}

void comm_set_type_motor(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_type_motor() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 100}); // [cur, 0x64]

    std::cin >> std::dec >> cur;
    if (cur != 0 && cur != 255) {
        throw std::runtime_error("comm_set_type_motor() invalid " + std::to_string(cur));
    }
    set_data(dst, {(uint8_t)cur}); // [cur]
}

void comm_set_availability_stopper(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_availability_stopper() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 101}); // [cur, 0x65]

    std::cin >> std::dec >> cur;
    if (cur != 0 && cur != 255) {
        throw std::runtime_error("comm_set_availability_stopper() invalid " + std::to_string(cur));
    }
    set_data(dst, {(uint8_t)cur}); // [cur]

}

void comm_set_end_sensors(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_end_sensors() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 102}); // [cur, 0x66]

    std::cin >> std::dec >> cur;
    if (cur != 0 && cur != 255) {
        throw std::runtime_error("comm_set_end_sensors() invalid " + std::to_string(cur));
    }
    set_data(dst, {(uint8_t)cur}); // [cur]
}

void comm_set_availability_increment_encoder(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_availability_increment_encoder() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 103}); // [cur, 0x67]

    std::cin >> std::dec >> cur;
    if (cur != 0 && cur != 255) {
        throw std::runtime_error("comm_set_availability_increment_encoder() invalid " + std::to_string(cur));
    }
    set_data(dst, {(uint8_t)cur}); // [cur]
}

void comm_set_availability_absolute_encoder(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_availability_absolute_encoder() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 104}); // [cur, 0x68]

    std::cin >> std::dec >> cur;
    if (cur != 0 && cur != 255) {
        throw std::runtime_error("comm_set_availability_absolute_encoder() invalid " + std::to_string(cur));
    }
    set_data(dst, {(uint8_t)cur}); // [cur]
}

void comm_set_availability_potentiometer(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_availability_potentiometer() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 105}); // [cur, 0x69]

    std::cin >> std::dec >> cur;
    if (cur != 0 && cur != 255) {
        throw std::runtime_error("comm_set_availability_potentiometer() invalid " + std::to_string(cur));
    }
    set_data(dst, {(uint8_t)cur}); // [cur]
}

void comm_set_microstep_mode(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_microstep_mode() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 106}); // [cur, 0x6A]

    uint16_t num;
    std::cin >> std::dec >> num;

    // check num is power of two & num > 0 & num <= 512
    if ((num > 0) && ((num & (num - 1)) == 0) && num <= 512) {
        throw std::runtime_error("comm_set_microstep_mode() invalid " + std::to_string(cur));
    }

    auto byte1 = (uint8_t)num;
    auto byte2 = (uint8_t)(num >> 8);

    set_data(dst, {byte2, byte1}); //[byte2, byte1]
}

void comm_set_maximum_speed_BLDC(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_maximum_speed_BLDC() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 107}); // [cur, 0x6B]

    uint16_t num;
    std::cin >> std::dec >> num;

    auto byte1 = (uint8_t)num;
    auto byte2 = (uint8_t)(num >> 8);

    set_data(dst, {byte2, byte1}); //[byte2, byte1]
}

void comm_set_boost_speed_BLDC(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_boost_speed_BLDC() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 108}); // [cur, 0x6C]

    uint16_t num;
    std::cin >> std::dec >> num;

    auto byte1 = (uint8_t)num;
    auto byte2 = (uint8_t)(num >> 8);

    set_data(dst, {byte2, byte1}); //[byte2, byte1]
}

void comm_set_quantity_tact_encoder(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_quantity_tact_encoder() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 109}); // [cur, 0x6D]

    std::cin >> std::dec >> cur;
    if (cur != 0 && cur != 255) {
        throw std::runtime_error("comm_set_quantity_tact_encoder() invalid " + std::to_string(cur));
    }

    uint16_t num;
    std::cin >> std::dec >> num;

    auto byte1 = (uint8_t)num;
    auto byte2 = (uint8_t)(num >> 8);

    set_data(dst, {(uint8_t)cur, byte2, byte1}); //[cur, byte2, byte1]
}

void comm_set_brake_condition(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_set_brake_condition() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 110}); // [cur, 0x6E]

    std::cin >> std::dec >> cur;
    if (cur != 0 && cur != 255) {
        throw std::runtime_error("comm_set_brake_condition() invalid " + std::to_string(cur));
    }
    set_data(dst, {(uint8_t)cur}); // [cur]
}

void comm_set_reduction_level(modbus_msg_t &dst){
    throw std::runtime_error("unimplemented method comm_set_reduction_level");
}

//*****SET COMMANDS*****


//*****REQUEST COMMANDS*****

void comm_req_calibration(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_req_calibration() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 65}); // [cur, 0x41]
}

void comm_req_firmware_and_configuration(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_req_firmware_and_configuration() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 66}); // [cur, 0x42]
}

void comm_req_diagnostic(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_req_diagnostic() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 67}); // [cur, 0x43]
}

void comm_req_read_id(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_req_read_id() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 17}); // [cur, 0x11]
}

void comm_req_quest_condition_sensor(modbus_msg_t &dst) {
    uint16_t cur;

    std::cin >> std::dec >> cur;
    if (cur < 1 || cur > 247) {
        throw std::runtime_error("comm_req_quest_condition_sensor() invalid " + std::to_string(cur));
    }
    set_header(dst, {(uint8_t)cur, 72}); // [cur, 0x48]
}

//*****REQUEST COMMANDS*****


//*****PROCESS COMMANDS*****

void comm_proc_displacement_in_position(modbus_msg_t &dst) {
    throw std::runtime_error("unimplemented method comm_proc_displacement_in_position");
}

void comm_proc_start_stop_velocity(modbus_msg_t &dst) {
    throw std::runtime_error("unimplemented method comm_proc_start_stop_velocity");
}

//*****PROCESS COMMANDS*****

}
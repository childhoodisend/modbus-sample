#ifndef CLIENT_SERVER_MODBUS_MSG_H
#define CLIENT_SERVER_MODBUS_MSG_H

#include <utility>
#include <vector>
#include <cstdint>

namespace modbus_conv {
/**
 * header -- vector of 2 bytes (address + function code)
 * data -- vector of 0 - 252 bytes
 * crc -- vector of 2 bytes
 */
struct modbus_msg_t {
public:
    modbus_msg_t(const std::vector<uint8_t>& h, const std::vector<uint8_t>& d, const std::vector<uint8_t>& c);
    modbus_msg_t() = default;
    ~modbus_msg_t() = default;

    std::vector<uint8_t> header{};
    std::vector<uint8_t> data{};
    std::vector<uint8_t> crc{};
};

/**
 * set header to msg
 * @param msg
 * @param addr
 * @param code
 */
void set_header(modbus_msg_t& msg, const std::vector<uint8_t> &src);

/**
 * set data to msg
 * @param msg
 * @param src
 */
void set_data(modbus_msg_t& msg, const std::vector<uint8_t>& src);

/**
 * set crc to msg
 * @param msg
 * @param src
 */
void set_crc(modbus_msg_t& msg, const std::vector<uint8_t>& src);

/**
 * @param msg
 * @return header vector
 */
std::vector<uint8_t> get_header(const modbus_msg_t& msg);

/**
 * @param msg
 * @return data vector
 */
std::vector<uint8_t> get_data(const modbus_msg_t& msg);

/**
 * @param msg
 * @return crc vector
 */
std::vector<uint8_t> get_crc(const modbus_msg_t& msg);


/**
 * validate msg
 * header must be 2 bytes
 * data must be 0-252 bytes
 * crc must be 2 bytes
 * etc...
 * @param msg
 */
void validate_msg(const modbus_msg_t& msg);


void print_msg(const modbus_msg_t& msg);

}

#endif //CLIENT_SERVER_MODBUS_MSG_H

#ifndef CLIENT_SERVER_MODBUS_COMMANDS_H
#define CLIENT_SERVER_MODBUS_COMMANDS_H

#include "modbus_msg.h"

#include <vector>

namespace modbus_conv {

void set_frame(modbus_msg_t &dst);
void set_frame(modbus_msg_t &dst, const std::vector<uint8_t> &src, bool crc_required = false);

//-----SET COMMANDS-----

void comm_set_slave(modbus_msg_t &dst); // 0

void comm_set_baud(modbus_msg_t &dst); // 1

void comm_set_type_motor(modbus_msg_t &dst); // 2

void comm_set_availability_stopper(modbus_msg_t &dst); // 3

void comm_set_end_sensors(modbus_msg_t &dst); // 4

void comm_set_availability_increment_encoder(modbus_msg_t &dst); // 5

void comm_set_availability_absolute_encoder(modbus_msg_t &dst); // 6

void comm_set_availability_potentiometer(modbus_msg_t &dst); // 7

void comm_set_microstep_mode(modbus_msg_t &dst); // 8

void comm_set_maximum_speed_BLDC(modbus_msg_t &dst); // 9

void comm_set_boost_speed_BLDC(modbus_msg_t &dst); // 10

void comm_set_quantity_tact_encoder(modbus_msg_t &dst); // 11

void comm_set_brake_condition(modbus_msg_t &dst); // 12

void comm_set_reduction_level(modbus_msg_t &dst); // 13

//*****SET COMMANDS*****


//*****REQUEST COMMANDS*****

void comm_req_calibration(modbus_msg_t &dst); // 0

void comm_req_firmware_and_configuration(modbus_msg_t &dst); // 1

void comm_req_diagnostic(modbus_msg_t &dst); // 2

void comm_req_read_id(modbus_msg_t &dst); // 3

void comm_req_quest_condition_sensor(modbus_msg_t &dst); // 4

//*****REQUEST COMMANDS*****


//*****PROCESS COMMANDS*****

void comm_proc_displacement_in_position(modbus_msg_t &dst); // 0

void comm_proc_start_stop_velocity(modbus_msg_t &dst); // 1

//*****PROCESS COMMANDS*****



}

#endif //CLIENT_SERVER_MODBUS_COMMANDS_H

#ifndef CLIENT_SERVER_GLOBALS_H
#define CLIENT_SERVER_GLOBALS_H

#include <vector>
#include <unordered_map>
#include <string>

//TODO fill the codes
// set brake condition
// set speed reduction level

static const std::vector<uint8_t> codes = {0x0A, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6A, 0x6B, 0x6C, 0x6D, // set
                                           0x41, 0x42, 0x43, 0x11, 0x48, // request
                                           0x44, 0x46}; // process

static std::unordered_map<std::string, uint8_t> set_commands{{"set slave",                          0},
                                                             {"set baud",                           1},
                                                             {"set type motor",                     2},
                                                             {"set availability stopper",           3},
                                                             {"set end sensors",                    4},
                                                             {"set availability increment encoder", 5},
                                                             {"set availability absolute_encoder",  6},
                                                             {"set availability potentiometer",     7},
                                                             {"set microstep mode",                 8},
                                                             {"set maximum speed BLDC",             9},
                                                             {"set boost speed BLDC",               10},
                                                             {"set quantity tact encoder",          11},
                                                             {"set brake condition",                12},
                                                             {"set reduction level",                13}};

static std::unordered_map<std::string, uint8_t> req_commands{{"calibration",                        0},
                                                             {"firmware and configuration request", 1},
                                                             {"diagnostic",                         2},
                                                             {"read id",                            3},
                                                             {"quest condition sensor",             4}};

static std::unordered_map<std::string, uint8_t> proc_commands{{"displacement in position", 0},
                                                              {"start stop velocity",      1}};

#endif //CLIENT_SERVER_GLOBALS_H
